﻿using Xamarin.Forms;

namespace Calicosol
{
    public partial class DemoApp : Application
    {
        public DemoApp()
        {
            InitializeComponent();
            var page = new MainPage();
            page.BindingContext = new MainViewModel(this);
            MainPage = new NavigationPage(page);
        }

        public async void NavigateTo(Page page)
        {
            await ((NavigationPage)MainPage).PushAsync(page);
        }

        private SubPage _subPage = null;
        public void GoToSubPage()
        {
            if (_subPage == null)
            {
                _subPage = new SubPage();
                _subPage.BindingContext = new MainViewModel(this);
            }
            NavigateTo(_subPage);
        }
    }
}
