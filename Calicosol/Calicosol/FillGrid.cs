﻿using System.Collections;

using Xamarin.Forms;

namespace Calicosol
{
    public class FillGrid : Grid
    {
        public FillGrid()
        {
            ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
        }

        #region ItemsSource Object
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create<FillGrid, IEnumerable>(
                ctrl => ctrl.ItemsSource,
                defaultValue: default(IEnumerable),
                defaultBindingMode: BindingMode.OneWay,
                propertyChanged: OnItemsSourceChanged
            );
        private static void OnItemsSourceChanged(BindableObject bindable, IEnumerable oldValue, IEnumerable newValue)
        {
            var ctrl = (FillGrid)bindable;
            if (ctrl.ItemTemplate != null)
            {
                UpdateGrid(ctrl, newValue, ctrl.ItemTemplate);
            }
        }
        #endregion

        #region ItemTemplate Object
        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }
        public static readonly BindableProperty ItemTemplateProperty =
            BindableProperty.Create<FillGrid, DataTemplate>(
                ctrl => ctrl.ItemTemplate,
                defaultValue: null,
                defaultBindingMode: BindingMode.OneWay,
                propertyChanged: OnItemTemplateChanged);
        private static void OnItemTemplateChanged(BindableObject bindable, DataTemplate oldValue, DataTemplate newValue)
        {
            var ctrl = (FillGrid)bindable;
            if (ctrl.ItemsSource != null)
            {
                UpdateGrid(ctrl, ctrl.ItemsSource, newValue);
            }
        }
        #endregion

        private static void UpdateGrid(FillGrid grid, IEnumerable source, DataTemplate template)
        {
            grid.Children.Clear();
            var i = 0;
            foreach(object o in source)
            {
                var column = i % 3;
                var row = i / 3;
                var view = (View)template.CreateContent();

                view.BindingContext = o;
                grid.Children.Add(view, column, row);
                grid.ChildRemoved += OnChildRemoved;

                ++i;
                if (i == 9) break;
            }
        }

        private static void OnChildRemoved(object sender, ElementEventArgs args)
        {
            args.Element.BindingContext = null;
        }
    }
}
