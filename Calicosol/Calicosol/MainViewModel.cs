﻿using System.Windows.Input;
using Xamarin.Forms;
using System.Collections;
using System.Collections.Generic;

namespace Calicosol
{
    public class MainViewModel : ViewModelBase
    {
        private List<ColorModel> colorList1 = new List<ColorModel>(){
                new ColorModel(Color.Aqua, "Aqua", Color.Black),
                new ColorModel(Color.Black, "Black", Color.White),
                new ColorModel(Color.Blue, "Blue", Color.White),
                new ColorModel(Color.Fuchsia, "Fuchsia", Color.Black),
                new ColorModel(Color.Gray, "Gray", Color.Black),
                new ColorModel(Color.Green, "Green", Color.Black),
                new ColorModel(Color.Lime, "Lime", Color.Black),
                new ColorModel(Color.Maroon, "Maroon", Color.White),
                new ColorModel(Color.Navy, "Navy", Color.White),
            };
        private List<ColorModel> colorList2 = new List<ColorModel>(){
                new ColorModel(Color.Olive, "Olive", Color.Black),
                new ColorModel(Color.Pink, "Pink", Color.Black),
                new ColorModel(Color.Purple, "Purple", Color.White),
                new ColorModel(Color.Red, "Red", Color.Black),
                new ColorModel(Color.Silver, "Silver", Color.Black),
                new ColorModel(Color.Teal, "Teal", Color.Black),
                new ColorModel(Color.White, "White", Color.Black),
                new ColorModel(Color.Yellow, "Yellow", Color.Black),
                new ColorModel(Color.Transparent, "Transparent", Color.Black),
            };

        public MainViewModel(DemoApp app) : base(app)
        {
            Title = "Main Page";
            ColorList = colorList1;
            SubPageCommand = new Command(nothing =>
            {
                Application.GoToSubPage();
            });
            SwitchCommand = new Command(nothing =>
            {
                if (ColorList == colorList1)
                    ColorList = colorList2;
                else
                    ColorList = colorList1;
            });
            
        }

        public ICommand SubPageCommand { get; }

        public ICommand SwitchCommand{ get; }

        private IEnumerable colorList;
        public IEnumerable ColorList
        {
            get
            {
                return colorList;
            }
            set
            {
                if (colorList != value)
                {
                    colorList = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
