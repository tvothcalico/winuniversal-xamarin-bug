﻿
namespace Calicosol
{
    public static class StringExtensions
    {
        public static string AppendIfNotNullOrEmpty(this string value, string append, bool newLine = true, string seperator = "")
        {
            if (value == null)
            {
                value = "";
            }
          
            var result = value;
            if (append != null && append.Trim().Length > 0)
            {
                if (newLine)
                {
                    result = value + "\n" + append;
                }
                else
                {
                    result = value + seperator + append;
                }

            }
            return result;
        }
    }
}
