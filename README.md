The custom element in this project (FormGrid) is a simplified version of the code to show the issue.  It has 2 bindable properties; an IEnumerable ItemsSource that allows the end user to pass in a list of items and an ItemTemplate that allows the end user to specify a how they would like the design to look (much like ListView).  The FormGrid will layout a 3x3 grid that will fill the available space and pace items in the blocks.

The simplest steps to reproduce the crashes are as follows...

1. Start the app.
2. Load the sub view with the "Sub-Page" button.
3. Use the back icon to navigate to the main page.
4. Load the sub view with the "Sub-Page" button again.
5. Switch the data in the IEnumerable via the "Switch Data" button.

The problem appears to come in when loading the SubView page for the second time. In DemoApp.xaml.cs there is a method GoToSubPage() where the different methods of loading the subpage causes different issues in Windows Universal (any method works in Android, iOs, and WinPhone).

When the code is in the current state (acting as if a single instance is used for both the View and the ViewModel) the following crash occurs when switching the data on the sub page.

Code:

```
#!c#

        private SubPage _subPage = null;
        public void GoToSubPage()
        {
            if (_subPage == null)
            {
                _subPage = new SubPage();
                _subPage.BindingContext = new MainViewModel(this);
            }
            NavigateTo(_subPage);
        }
```

Crash:

```
#!c#

System.ArgumentException was unhandled by user code
  HResult=-2147024809
  Message=Value does not fall within the expected range.
  Source=Windows
  StackTrace:
       at Windows.UI.Xaml.Controls.Border.put_Child(UIElement value)
       at Xamarin.Forms.Platform.WinRT.FrameRenderer.PackChild()
       at Xamarin.Forms.Platform.WinRT.FrameRenderer.OnElementChanged(ElementChangedEventArgs`1 e)
       at Xamarin.Forms.Platform.WinRT.VisualElementRenderer`2.SetElement(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.RendererFactory.CreateRenderer(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.VisualElementPackager.OnChildAdded(Object sender, ElementEventArgs e)
       at System.EventHandler`1.Invoke(Object sender, TEventArgs e)
       at Xamarin.Forms.Element.OnChildAdded(Element child)
       at Xamarin.Forms.Layout.OnInternalAdded(View view)
       at Xamarin.Forms.Layout.InternalChildrenOnCollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
       at System.Collections.Specialized.NotifyCollectionChangedEventHandler.Invoke(Object sender, NotifyCollectionChangedEventArgs e)
       at System.Collections.ObjectModel.ObservableCollection`1.OnCollectionChanged(NotifyCollectionChangedEventArgs e)
       at System.Collections.ObjectModel.ObservableCollection`1.InsertItem(Int32 index, T item)
       at System.Collections.ObjectModel.Collection`1.Add(T item)
       at Xamarin.Forms.ObservableWrapper`2.Add(TRestrict item)
       at Xamarin.Forms.Grid.GridElementCollection.Add(View view, Int32 left, Int32 right, Int32 top, Int32 bottom)
       at Xamarin.Forms.Grid.GridElementCollection.Add(View view, Int32 left, Int32 top)
       at Calicosol.FillGrid.UpdateGrid(FillGrid grid, IEnumerable source, DataTemplate template)
       at Calicosol.FillGrid.OnItemsSourceChanged(BindableObject bindable, IEnumerable oldValue, IEnumerable newValue)
       at Xamarin.Forms.BindableProperty.<>c__DisplayClass5`2.<Create>b__1(BindableObject bindable, Object oldValue, Object newValue)
       at Xamarin.Forms.BindableObject.SetValueActual(BindableProperty property, BindablePropertyContext context, Object value, Boolean currentlyApplying, SetValueFlags attributes, Boolean silent)
       at Xamarin.Forms.BindableObject.SetValueCore(BindableProperty property, Object value, SetValueFlags attributes, SetValuePrivateFlags privateAttributes)
       at Xamarin.Forms.BindingExpression.ApplyCore(Object sourceObject, BindableObject target, BindableProperty property, Boolean fromTarget)
       at Xamarin.Forms.BindingExpression.Apply(Boolean fromTarget)
       at Xamarin.Forms.BindingExpression.BindingExpressionPart.<PropertyChanged>b__c()
       at Xamarin.Forms.Platform.WinRT.WindowsBasePlatformServices.<>c__DisplayClass1.<BeginInvokeOnMainThread>b__0()
  InnerException: 
```


When the code is in the following state (acting as if a single instance is used for the View but a new ViewModel is created each time) the following crash occurs when loading the sub page for the second time.

Code: 

```
#!c#

        private SubPage _subPage = null;
        public void GoToSubPage()
        {
            if (_subPage == null)
            {
                _subPage = new SubPage();
            }
            _subPage.BindingContext = new MainViewModel(this);
            NavigateTo(_subPage);
        }
```

Crash:

```
#!c#

System.ArgumentException was unhandled by user code
  HResult=-2147024809
  Message=Value does not fall within the expected range.
  Source=Windows
  StackTrace:
       at Windows.UI.Xaml.Controls.Border.put_Child(UIElement value)
       at Xamarin.Forms.Platform.WinRT.FrameRenderer.PackChild()
       at Xamarin.Forms.Platform.WinRT.FrameRenderer.OnElementChanged(ElementChangedEventArgs`1 e)
       at Xamarin.Forms.Platform.WinRT.VisualElementRenderer`2.SetElement(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.RendererFactory.CreateRenderer(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.VisualElementPackager.OnChildAdded(Object sender, ElementEventArgs e)
       at Xamarin.Forms.Platform.WinRT.VisualElementPackager.Load()
       at Xamarin.Forms.Platform.WinRT.VisualElementRenderer`2.SetElement(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.RendererFactory.CreateRenderer(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.VisualElementPackager.OnChildAdded(Object sender, ElementEventArgs e)
       at Xamarin.Forms.Platform.WinRT.VisualElementPackager.Load()
       at Xamarin.Forms.Platform.WinRT.VisualElementRenderer`2.SetElement(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.RendererFactory.CreateRenderer(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.VisualElementPackager.OnChildAdded(Object sender, ElementEventArgs e)
       at Xamarin.Forms.Platform.WinRT.VisualElementPackager.Load()
       at Xamarin.Forms.Platform.WinRT.PageRenderer.SetElement(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.RendererFactory.CreateRenderer(VisualElement element)
       at Xamarin.Forms.Platform.WinRT.VisualElementExtensions.GetOrCreateRenderer(VisualElement self)
       at Xamarin.Forms.Platform.WinRT.NavigationPageRenderer.SetPage(Page page, Boolean isAnimated, Boolean isPopping)
       at Xamarin.Forms.Platform.WinRT.NavigationPageRenderer.OnPushRequested(Object sender, NavigationRequestedEventArgs e)
       at Xamarin.Forms.NavigationPage.<PushAsyncInner>d__16.MoveNext()
    --- End of stack trace from previous location where exception was thrown ---
       at System.Runtime.CompilerServices.TaskAwaiter.ThrowForNonSuccess(Task task)
       at System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task)
       at Xamarin.Forms.NavigationPage.<PushAsync>d__11.MoveNext()
    --- End of stack trace from previous location where exception was thrown ---
       at System.Runtime.CompilerServices.TaskAwaiter.ThrowForNonSuccess(Task task)
       at System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task)
       at System.Runtime.CompilerServices.TaskAwaiter.GetResult()
       at Calicosol.DemoApp.<NavigateTo>d__1.MoveNext()
  InnerException: 
```


When the code is in the following state (acting as if a new instance is used for the View and ViewModel is created each time) there is no crash.  While we are aware of this workaround it is not desired in our larger application.

Code:

```
#!c#

        private SubPage _subPage = null;
        public void GoToSubPage()
        {
            _subPage = new SubPage();
            _subPage.BindingContext = new MainViewModel(this);
            NavigateTo(_subPage);
        }
```


Crash: NONE